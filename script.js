tab1 = [];
tab2 = [];
tab3 = [];
tab4 = [];

alert('Veuillez saisir 5 valeurs pour tab1 et tab2');

let n = 5
for (let i = 1; i <= n; i++) {
    let input = parseInt(prompt(`Veuillez saisir la valeur ${[i]} du tableau 1`));
    tab1.push(input);
}
console.log(tab1);

for (let i = 1; i <= n; i++) {
    let input2 = parseInt(prompt(`Veuillez saisir la valeur ${[i]} du tableau 2`));
    tab2.push(input2);
}
console.log(tab2)

// Creation du tab3 
for (let i = 0; i < tab1.length; i++) {
    if (!tab2.includes(tab1[i])) {
        tab3.push(tab1[i]);
    }
}
console.log(tab3);

// creation du tab4
for (let i = 0; i < tab2.length; i++) {
    if (tab1.includes(tab2[i])) {
        tab4.push(tab2[i]);
    } 
}
console.log(tab4);




// Exo 2 : 

let tab5Tri = [5, 3, 87, 1, -4, -99, 0];

function trierTableau(tab5){ 

    let m = tab5.length;

    for (let i = 0; i < m; i++) {
        for (let j = 0; j < m - 1 -i; j++) {
            if (tab5[j] > tab5[j + 1]) {
                // echanger les elements
                let temp = tab5[j];
                tab5[j] = tab5[j + 1];
                tab5[j + 1] = temp;
            }
            
        } 
    }
    return tab5;
    
}

let tabTri = trierTableau(tab5Tri);
console.log(tabTri);

// Max Min
function trouverMaxMin(tab5) {
    let max = tab5[0];
    let min = tab5[0];
    for (let i = 1; i < tab5.length; i++) {
      if (tab5[i] > max) {
        max = tab5[i];
      }
      if (tab5[i] < min) {
        min = tab5[i];
      }
    }
    return {max: max, min: min};
  }

  let tabMaxMin = trouverMaxMin(tab5Tri);
  console.log(tabMaxMin);